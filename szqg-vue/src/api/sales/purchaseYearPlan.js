import request from '@/utils/request'

// 查询年计划列表
export function listCustomePlan(query) {
  return request({
    url: '/sales/purchase/year/list',
    method: 'get',
    params: query
  })
}

// 查询年计划详细
export function getCustomePlan(yearSellPlanId) {
  return request({
    url: '/sales/purchase/year/' + yearSellPlanId,
    method: 'get'
  })
}

// 新增年计划
export function addCustomePlan(data) {
  return request({
    url: '/sales/purchase/year',
    method: 'post',
    data: data
  })
}

// 修改年计划
export function updateCustomePlan(data) {
  return request({
    url: '/sales/purchase/year',
    method: 'put',
    data: data
  })
}

// 删除年计划
export function delCustomePlan(yearSellPlanId) {
  return request({
    url: '/sales/purchase/year/' + yearSellPlanId,
    method: 'delete'
  })
}

// 导出年计划
export function exportCustomePlan(query) {
  return request({
    url: '/sales/purchase/year/export',
    method: 'get',
    params: query
  })
}

export function updateCustomePlanStatus(yearSellPlanId,status) {
  return request({
    url: '/sales/custome/updateCustomeStatus/' + yearSellPlanId+"/"+status,
    method: 'get'
  })
}


