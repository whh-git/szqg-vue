import request from '@/utils/request'

// 查询采购月计划列表
export function listMonthPlan(query) {
  return request({
    url: '/sales/monthPlan/list',
    method: 'get',
    params: query
  })
}

// 查询采购月计划详细
export function getMonthPlan(monthPurchasePlanId) {
  return request({
    url: '/sales/monthPlan/' + monthPurchasePlanId,
    method: 'get'
  })
}

// 编辑采购月计划
export function updateMonthPlan(data) {
  return request({
    url: '/sales/monthPlan',
    method: 'put',
    data: data
  })
}



