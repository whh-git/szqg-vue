import request from '@/utils/request'

// 查询交接凭证-拉运列表
export function listCart(query) {
  return request({
    url: '/sales/cart/list',
    method: 'get',
    params: query
  })
}

// 查询交接凭证-拉运详细
export function getCart(handoverCartId) {
  return request({
    url: '/sales/cart/' + handoverCartId,
    method: 'get'
  })
}

// 新增交接凭证-拉运
export function addCart(data) {
  return request({
    url: '/sales/cart',
    method: 'post',
    data: data
  })
}

// 修改交接凭证-拉运
export function updateCart(data) {
  return request({
    url: '/sales/cart',
    method: 'put',
    data: data
  })
}

// 删除交接凭证-拉运
export function delCart(handoverCartId) {
  return request({
    url: '/sales/cart/' + handoverCartId,
    method: 'delete'
  })
}


// 确认计量交接凭证信息
export function updateCartStatus(handoverCartId){
  return request({
    url: '/sales/cart/updateCartStatus/' + handoverCartId,
    method: 'get'
  })
  
}