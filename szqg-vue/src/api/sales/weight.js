import request from '@/utils/request'

// 查询称重计量单列表
export function listDoc(query) {
  return request({
    url: '/sales/doc/list',
    method: 'get',
    params: query
  })
}

// 查询称重计量单详细
export function getDoc(weightId) {
  return request({
    url: '/sales/doc/' + weightId,
    method: 'get'
  })
}

// 新增称重计量单
export function addDoc(data) {
  return request({
    url: '/sales/doc',
    method: 'post',
    data: data
  })
}

// 修改称重计量单
export function updateDoc(data) {
  return request({
    url: '/sales/doc',
    method: 'put',
    data: data
  })
}

// 删除称重计量单
export function delDoc(weightId) {
  return request({
    url: '/sales/doc/' + weightId,
    method: 'delete'
  })
}


// 确认过磅信息
export function updateWeightStatus(weightId){
  return request({
    url: '/sales/doc/updateWeightStatus/' + weightId,
    method: 'get'
  })
}
