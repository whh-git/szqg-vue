import request from '@/utils/request'

// 查询采购交接凭证-拉运列表
export function listVoucherCart(query) {
  return request({
    url: '/sales/voucherCart/list',
    method: 'get',
    params: query
  })
}

// 查询采购交接凭证-拉运详细
export function getCart(handoverPurchaseCartId) {
  return request({
    url: '/sales/voucherCart/' + handoverPurchaseCartId,
    method: 'get'
  })
}

// 编辑采购计量交接拉运凭证
export function updateCart(data) {
  return request({
    url: '/sales/voucherCart',
    method: 'put',
    data: data
  })
  
}

