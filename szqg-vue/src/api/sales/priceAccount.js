import request from '@/utils/request'

// 查询采购价格列表
export function listPrice(query) {
  return request({
    url: '/sales/price/list',
    method: 'get',
    params: query
  })
}

// 查询采购价格详细
export function getPrice(purchasePriceId) {
  return request({
    url: '/sales/price/' + purchasePriceId,
    method: 'get'
  })
}

// 新增采购价格
export function addPrice(data) {
  return request({
    url: '/sales/price',
    method: 'post',
    data: data
  })
}

// 修改采购价格
export function updatePrice(data) {
  return request({
    url: '/sales/price',
    method: 'put',
    data: data
  })
}

export function submitPrice(data) {
  return request({
    url: '/sales/price/submitPrice',
    method: 'post',
    data: data
  })
}

// 删除采购价格
export function delPrice(purchasePriceId) {
  return request({
    url: '/sales/price/' + purchasePriceId,
    method: 'delete'
  })
}

export function countHandover(data) {
  return request({
    url: '/sales/price/countHandover',
    method: 'post',
    data:data
  })
}


countHandover

// 导出采购价格
export function exportPrice(query) {
  return request({
    url: '/sales/price/export',
    method: 'get',
    params: query
  })
}

export function getProductPrice(query) {
  return request({
    url: '/sales/price/getPriceByProductId',
    method: 'get',
    params: query
  })
}
