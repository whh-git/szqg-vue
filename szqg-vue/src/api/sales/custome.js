import request from '@/utils/request'

// 查询客户列表
export function listCustome(query) {
  return request({
    url: '/sales/custome/list',
    method: 'get',
    params: query
  })
}

// 查询客户详细
export function getCustome(customeId) {
  return request({
    url: '/sales/custome/' + customeId,
    method: 'get'
  })
}

// 新增客户
export function addCustome(data) {
  return request({
    url: '/sales/custome',
    method: 'post',
    data: data
  })
}

// 修改客户
export function updateCustome(data) {
  return request({
    url: '/sales/custome',
    method: 'put',
    data: data
  })
}

// 删除客户
export function delCustome(customeId) {
  return request({
    url: '/sales/custome/' + customeId,
    method: 'delete'
  })
}

// 导出客户
export function exportCustome(query) {
  return request({
    url: '/sales/custome/export',
    method: 'get',
    params: query
  })
}

export function updateCustomeStatus(data) {
  return request({
    url: '/sales/custome/updateCustomeStatus',
    method: 'post',
    data:data
  })
}
