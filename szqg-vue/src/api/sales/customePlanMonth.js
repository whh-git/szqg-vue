import request from '@/utils/request'

// 查询月计划列表
export function listCustomePlan(query) {
  return request({
    url: '/sales/custome/planMonth/list',
    method: 'get',
    params: query
  })
}

// 查询月计划列表
export function listCustomeReportPlan(query) {
  return request({
    url: '/sales/yearController/Reportlist',
    method: 'get',
    params: query
  })
}

// 查询月计划详细
export function getCustomePlan(annualSalesPlanId) {
  return request({
    url: '/sales/custome/planMonth/' + annualSalesPlanId,
    method: 'get'
  })
}

// 新增月计划
export function addCustomePlan(data) {
  return request({
    url: '/sales/custome/planMonth',
    method: 'post',
    data: data
  })
}

// 修改月计划
export function updateCustomePlan(data) {
  return request({
    url: '/sales/custome/planMonth',
    method: 'put',
    data: data
  })
}

// 删除月计划
export function delCustomePlan(annualSalesPlanId) {
  return request({
    url: '/sales/custome/planMonth/' + annualSalesPlanId,
    method: 'delete'
  })
}

// 导出月计划
export function exportCustomePlan(query) {
  return request({
    url: '/sales/custome/planMonth/export',
    method: 'get',
    params: query
  })
}

export function updateCustomePlanStatus(annualSalesPlanId,status) {
  return request({
    url: '/sales/custome/updateCustomeStatus/' + annualSalesPlanId+"/"+status,
    method: 'get'
  })
}


export function getSalesMonthList(query) {
  return request({
    url: '/sales/count/getSalesMonthList',
    method: 'get',
    params: query
  })
}



