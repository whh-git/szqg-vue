import request from '@/utils/request'

// 查询销售合同列表
export function listContract(query) {
  return request({
    url: '/sales/contract/list',
    method: 'get',
    params: query
  })
}

//查询员工信息表
export function employeeList(){
  return request({
    url: '/sales/contract/employeeList',
    method: 'get'
  })
}
// 查询销售合同详细
export function getContract(id) {
  return request({
    url: '/sales/contract/' + id,
    method: 'get'
  })
}

//查询父机构的缩写
export function checkOrgPre(orgId) {
  return request({
    url: '/sales/contract/org/' + orgId,
    method: 'get'
  })
}
// 新增销售合同
export function addContract(data) {
  return request({
    url: '/sales/contract',
    method: 'post',
    data: data
  })
}

// 修改销售合同
export function updateContract(data) {
  return request({
    url: '/sales/contract',
    method: 'put',
    data: data
  })
}

// 删除销售合同
export function delContract(ids) {
  return request({
    url: '/sales/contract/delete/' + ids,
    method: 'put'
  })
}

//确认合同台账
export function dealConfirm(data){
  return request({
    url: '/sales/contract/dealConfim',
    method: 'post',
    data:data
  })
}

// 导出销售合同
export function exportContract(query) {
  return request({
    url: '/sales/contract/export',
    method: 'get',
    params: query
  })
}

//终结获取合同详情
export function getContractByEnd(id) {
  return request({
    url: '/sales/contract/getContractByEnd/' + id,
    method: 'get'
  })
}

export function updateContractByEnd(data) {
  return request({
    url: '/sales/contract/updateContractByEnd',
    method: 'post',
    data: data
  })
}

export function getContractExecuteList(query) {
  return request({
    url: '/sales/contract/getContractExecuteList',
    method: 'get',
    params: query
  })
}

export function getContractExecuteListV2(query) {
  return request({
    url: '/sales/contract/getContractExecuteListV2',
    method: 'get',
    params: query
  })
}

export function getContractEndList(query) {
  return request({
    url: '/sales/contract/getContractEndList',
    method: 'get',
    params: query
  })
}


export function disarmWarn(ids) {
  return request({
    url: '/sales/contract/disarmWarn/' + ids,
    method: 'get'
  })
}