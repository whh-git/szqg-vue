import request from '@/utils/request';


// 查询设备的检测因子数据
export function selectPriceByItem(object) {
  return request({
    url: '/sales/price/selectPriceByItem',
    method: 'put',
    data: object
  })
  
}

export function selectPriceByItemV2(data) {
  return request({
    url: '/sales/price/selectPriceByItemV2',
    method: 'post',
    data:data
  })
}

