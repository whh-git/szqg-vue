import request from '@/utils/request'

/**
 * 查询
 */
export function getOrgCompany() {
  return request({
    url: '/sales/query/getOrgCompany',
    method: 'get', 
  })
}

export function getProductTransport(productId) {
  return request({
    url: '/sales/query/getProductTransport/'+productId,
    method: 'get', 
  })
}


export function getOrgCompanyByLogin() {
  return request({
    url: '/sales/query/getOrgCompanyByLogin',
    method: 'get',
  })
}