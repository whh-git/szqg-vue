import request from '@/utils/request'

// 查询客户资料附件列表
export function listFile(query) {
  return request({
    url: '/sales/file/list',
    method: 'get',
    params: query
  })
}

// 查询客户资料附件详细
export function getFile(customeProofId) {
  return request({
    url: '/sales/file/' + customeProofId,
    method: 'get'
  })
}

// 新增客户资料附件
export function addFile(data) {
  return request({
    url: '/sales/file',
    method: 'post',
    data: data
  })
}

// 修改客户资料附件
export function updateFile(data) {
  return request({
    url: '/sales/file',
    method: 'put',
    data: data
  })
}

// 删除客户资料附件
export function delFile(customeProofId) {
  return request({
    url: '/sales/file/' + customeProofId,
    method: 'delete'
  })
}

// 导出客户资料附件
export function exportFile(query) {
  return request({
    url: '/sales/file/export',
    method: 'get',
    params: query
  })
}

export function upload(data) {
  return request({
    url: '/sales/file/upload',
    method: 'post',
    data: data
  })
}
