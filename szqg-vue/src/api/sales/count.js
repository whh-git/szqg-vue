import request from '@/utils/request'

/**
 * 拉运汇总
 * @param {*} query 
 * @returns 
 */
export function listHandover(query) {
  return request({
    url: '/sales/count/list',
    method: 'get',
    params: query
  })
}

export function getTotalMoney(query) {
  return request({
    url: '/sales/count/getTotalMoney',
    method: 'get',
    params: query
  })
}

export function exportHandover(query) {
  return request({
    url: '/sales/count/export',
    method: 'get',
    params: query
  })
}

/**
 * 采购汇总表
 * @param {*} query 
 * @returns 
 */
export function getPurchaseList(query) {
  return request({
    url: '/sales/count/getPurchaseList',
    method: 'get',
    params: query
  })
}

export function getPurchaseExport(query) {
  return request({
    url: '/sales/count/getPurchaseExport',
    method: 'get',
    params: query
  })
}

export function getPurchaseTotalMoney(query) {
  return request({
    url: '/sales/count/getPurchaseTotalMoney',
    method: 'get',
    params: query
  })
}


/**
 * 销售汇总表
 * @param {*} query 
 * @returns 
 */
export function getSalesList(query) {
  return request({
    url: '/sales/count/getSalesList',
    method: 'get',
    params: query
  })
}

export function getExportSales(query) {
  return request({
    url: '/sales/count/getExportSales',
    method: 'get',
    params: query
  })
}

/**
 * 销售月报
 */
 export function getSalesMonthList(query) {
  return request({
    url: '/sales/count/getSalesMonthList',
    method: 'get',
    params: query
  })
}


export function getSalesMonthExport(query) {
  return request({
    url: '/sales/count/getSalesMonthExport',
    method: 'get',
    params: query
  })
}


export function getAgreementList(query) {
  return request({
    url: '/sales/count/getAgreementList',
    method: 'get',
    params: query
  })
}

export function getDefultAgreementList() {
  return request({
    url: '/sales/count/getDefultAgreementList',
    method: 'get',
  })
}

export function getOutOrderList(query) {
  return request({
    url: '/sales/count/getOutOrderList',
    method: 'get',
    params: query
  })
}
