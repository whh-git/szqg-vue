import request from '@/utils/request'

// 查询交接凭证-管道列表
export function listPipe(query) {
  return request({
    url: '/system/pipe/list',
    method: 'get',
    params: query
  })
}

export function InstrumentList(query) {
  return request({
    url: '/system/instrument/list',
    method: 'get',
    params: query
  })
}

// 查询交接凭证-管道详细
export function getPipe(handoverPipeId) {
  return request({
    url: '/system/pipe/' + handoverPipeId,
    method: 'get'
  })
}

// 新增交接凭证-管道
export function addPipe(data) {
  return request({
    url: '/system/pipe',
    method: 'post',
    data: data
  })
}

// 修改交接凭证-管道
export function updatePipe(data) {
  return request({
    url: '/system/pipe',
    method: 'put',
    data: data
  })
}

// 删除交接凭证-管道
export function delPipe(handoverPipeId) {
  return request({
    url: '/system/pipe/' + handoverPipeId,
    method: 'delete'
  })
}

// 导出交接凭证-管道
export function exportPipe(query) {
  return request({
    url: '/system/pipe/export',
    method: 'get',
    params: query
  })
}

// 确认过磅信息
export function updatePipeStatus(handoverPipeId){
  return request({
    url: '/system/pipe/updatePipeStatus/' + handoverPipeId,
    method: 'get'
  })
}

// 交易明细列表
listDetail
export function listDetail(query) {
  return request({
    url: '/system/pipe/detaillist',
    method: 'get',
    params: query
  })
}


// 交易明细列表
export function listPipeDe(query) {
  return request({
    url: '/system/pipeDetail/list',
    method: 'get',
    params: query
  })
}


export function getContractDetail(sellContractId) {
  return request({
    url: '/system/pipe/getContractDetail/'+sellContractId,
    method: 'get',
  })
}
