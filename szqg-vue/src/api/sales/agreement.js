import request from '@/utils/request'

// 查询计量交接协议列表
export function listAgreement(query) {
  return request({
    url: '/sales/agreement/list',
    method: 'get',
    params: query
  })
}
export function listPurchase(query){
  return request({
    url: '/sales/agreement/purchaseList',
    method: 'get',
    params: query
  })
}
export function productInfoById(productId){
  return request({
    url: '/sales/agreement/productInfo/'+productId,
    method: 'get'
  })
}


//查询父机构的缩写
export function checkOrgPre(orgId) {
  return request({
    url: '/sales/agreement/org/' + orgId,
    method: 'get'
  })
}
export function getContract(contractId) {
    return request({
      url: '/sales/contract/' + contractId,
      method: 'get'
    })
  }
  // 新增计量交接协议
export function addAgreement(data) {
  return request({
    url: '/sales/agreement',
    method: 'post',
    data: data
  })
}

// 查询计量交接协议详细
export function getAgreement(handoverAgreementId) {
  return request({
    url: '/sales/agreement/' + handoverAgreementId,
    method: 'get'
  })
}
export function  getProdectName(purchaseContractId){
  return request({
    url: '/sales/agreement/purchase/' + purchaseContractId,
    method: 'get'
  })
}

// 删除计量交接协议
export function delAgreement(handoverAgreementId) {
  return request({
    url: '/sales/agreement/' + handoverAgreementId,
    method: 'delete'
  })
}

// 修改计量交接协议
export function updateAgreement(data) {
  return request({
    url: '/sales/agreement',
    method: 'put',
    data: data
  })
}
//确认
export function dealConfirm(data) {
  return request({
    url: '/sales/agreement/dealConfim',
    method: 'post',
    data:data
  })
}


