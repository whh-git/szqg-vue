import request from '@/utils/request'
import { defaultFormat } from 'moment'

// 查询客户与产品关系列表
export function listRelation(query) {
  return request({
    url: '/system/relation/list',
    method: 'get',
    params: query
  })
}

// 查询客户与产品关系详细
export function getRelation(customeProductId) {
  return request({
    url: '/system/relation/' + customeProductId,
    method: 'get'
  })
}

// 新增客户与产品关系
export function addRelation(data) {
  return request({
    url: '/system/relation',
    method: 'post',
    data: data
  })
}

// 修改客户与产品关系
export function updateRelation(data) {
  return request({
    url: '/system/relation',
    method: 'put',
    data: data
  })
}

// 删除客户与产品关系
export function delRelation(customeProductId) {
  return request({
    url: '/system/relation/' + customeProductId,
    method: 'delete'
  })
}

// 导出客户与产品关系
export function exportRelation(query) {
  return request({
    url: '/system/relation/export',
    method: 'get',
    params: query
  })
}

export function getProductByCustome(customeId,productType,productId) {
  const data = {
    customeId,
    type:productType,
    productId
  }
  return request({
    url: '/system/product/getProductByCustome',
    method: 'post',
    data: data
  })
}

export function getProductByCustomeAndType(customeId,type) {
  const data = {
    customeId,
    type
  }
  return request({
    url: '/system/product/getProductByCustomeAndType',
    method: 'post',
    data: data
  })
}

export function getProduct(productId) {
  return request({
    url: '/system/product/' +productId,
    method: 'get'
  })
}

export function getProductQuery(data) {
  return request({
    url: '/system/relation/getProducAndType',
    method: 'post',
    data:data
  })
}

export function getProductByCustomeId() {
  return request({
    url: '/system/relation/getProductByCustomeId',
    method: 'get'
  })
}

export function getRelationProductByCustomeId(customeId) {
  return request({
    url: '/system/relation/getRelationProductByCustomeId/'+customeId,
    method: 'get'
  })
}
