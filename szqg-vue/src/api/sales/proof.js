import request from '@/utils/request'

// 查询附件列表
export function listProof(query) {
  return request({
    url: '/sales/proof/list',
    method: 'get',
    params: query
  })
}

// 查询附件详细
export function getProof(proofId) {
  return request({
    url: '/sales/proof/' + proofId,
    method: 'get'
  })
}

// 新增附件
export function addProof(data) {
  return request({
    url: '/sales/proof',
    method: 'post',
    data: data
  })
}

// 修改附件
export function updateProof(data) {
  return request({
    url: '/sales/proof',
    method: 'put',
    data: data
  })
}

// 删除附件
export function delProof(proofId) {
  return request({
    url: '/sales/proof/' + proofId,
    method: 'delete'
  })
}

// 导出附件
export function exportProof(query) {
  return request({
    url: '/system/proof/export',
    method: 'get',
    params: query
  })
}

