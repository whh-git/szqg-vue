import request from '@/utils/request'

// 查询结算单列表
export function listStatements(query) {
  return request({
    url: '/sales/statements/list',
    method: 'get',
    params: query
  })
}

// 查询结算单详细
export function getStatements(statementsId) {
  return request({
    url: '/sales/statements/' + statementsId,
    method: 'get'
  })
}

// 新增结算单
export function addStatements(data) {
  return request({
    url: '/sales/statements',
    method: 'post',
    data: data
  })
}

// 修改结算单
export function updateStatements(data) {
  return request({
    url: '/sales/statements',
    method: 'put',
    data: data
  })
}

// 删除结算单
export function delStatements(data) {
  return request({
    url: '/sales/statements/deleteStatements',
    method: 'post',
    data:data
  })
}

// 导出结算单
export function exportStatements(query) {
  return request({
    url: '/sales/statements/export',
    method: 'get',
    params: query
  })
}

export function getHandover(query) {
  return request({
    url: '/sales/statements/getHandoverList',
    method: 'get',
    params: query
  })
}

export function getProductByCustomeId(customeId) {
  return request({
    url: '/sales/statements/getProductByCustomeId/'+customeId,
    method: 'get',
  })
}

export function goBackStatement(data) {
  return request({
    url: '/sales/statements/goBackStatement',
    method: 'post',
    data: data
  })
}

export function submitStatement(data) {
  return request({
    url: '/sales/statements/submitStatement',
    method: 'post',
    data: data
  })
}

export function getStatementsInfo(data) {
  return request({
    url: '/sales/statements/getStatementsInfo',
    method: 'post',
    data:data
  })
}

export function getUpdateStatementsInfo(statementsId) {
  return request({
    url: '/sales/statements/getUpdateStatementsInfo/' + statementsId,
    method: 'get'
  })
}

export function selectInfoList(query) {
  return request({
    url: '/sales/statements/selectInfoList',
    method: 'get',
    params: query
  })
}



