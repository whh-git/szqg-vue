import request from '@/utils/request'

// 查询合同交易明细收款列表
export function listPayment(query) {
  return request({
    url: '/sales/payment/list',
    method: 'get',
    params: query
  })
}

export function contractDetailList(query) {
  return request({
    url: '/sales/payment/contractDetailList',
    method: 'get',
    params: query
  })
}

// 查询合同交易明细收款详细
export function getPayment(contractPaymentId) {
  return request({
    url: '/sales/payment/' + contractPaymentId,
    method: 'get'
  })
}

// 新增合同交易明细收款
export function addPayment(data) {
  return request({
    url: '/sales/payment',
    method: 'post',
    data: data
  })
}

// 修改合同交易明细收款
export function updatePayment(data) {
  return request({
    url: '/sales/payment',
    method: 'put',
    data: data
  })
}

// 删除合同交易明细收款
export function delPayment(contractPaymentId) {
  return request({
    url: '/sales/payment/' + contractPaymentId,
    method: 'delete'
  })
}

export function submitPayment(contractPaymentId) {
  return request({
    url: '/sales/payment/submitPayment/' + contractPaymentId,
    method: 'get'
  })
}

// 导出合同交易明细收款
export function exportPayment(query) {
  return request({
    url: '/sales/payment/export',
    method: 'get',
    params: query
  })
}