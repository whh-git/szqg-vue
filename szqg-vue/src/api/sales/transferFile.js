import request from '@/utils/request'

// 查询调拨单列表
export function listFile(query) {
  return request({
    url: '/sales/transferFile/list',
    method: 'get',
    params: query
  })
}

// 查询调拨单详细
export function getFile(transferId) {
  return request({
    url: '/sales/transferFile/' + transferId,
    method: 'get'
  })
}

// 新增调拨单
export function addFile(data) {
  return request({
    url: '/sales/transferFile',
    method: 'post',
    data: data
  })
}

// 修改调拨单
export function updateFile(data) {
  return request({
    url: '/sales/transferFile',
    method: 'put',
    data: data
  })
}

// 删除调拨单
export function delFile(transferId) {
  return request({
    url: '/sales/transferFile/' + transferId,
    method: 'delete'
  })
}

// 导出调拨单
export function exportFile(query) {
  return request({
    url: '/sales/transferFile/export',
    method: 'get',
    params: query
  })
}

export function getPaymentTotal(sellContractId) {
  return request({
    url: '/sales/transferFile/getPaymentTotal/' + sellContractId,
    method: 'get'
  })
}

export function getTransferById(transferId) {
  return request({
    url: '/sales/transferFile/getTransferById/' + transferId,
    method: 'get'
  })
}

// 称重计量单弹出调拨单列表
export function weightTransferList(query) {
  return request({
    url: '/sales/transferFile/weightTransferList',
    method: 'get',
    params: query
  })
}