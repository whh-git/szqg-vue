import request from '@/utils/request'

// 查询客户登录用户列表
export function listInfo(query) {
  return request({
    url: '/sales/info/list',
    method: 'get',
    params: query
  })
}

// 查询客户登录用户详细
export function getInfo(loginId) {
  return request({
    url: '/sales/info/' + loginId,
    method: 'get'
  })
}

// 新增客户登录用户
export function addInfo(data) {
  return request({
    url: '/sales/info',
    method: 'post',
    data: data
  })
}

// 修改客户登录用户
export function updateInfo(data) {
  return request({
    url: '/sales/info',
    method: 'put',
    data: data
  })
}

// 删除客户登录用户
export function delInfo(loginId) {
  return request({
    url: '/sales/info/' + loginId,
    method: 'delete'
  })
}

// 导出客户登录用户
export function exportInfo(query) {
  return request({
    url: '/sales/info/export',
    method: 'get',
    params: query
  })
}

export function resetPwdDefault(loginId) {
  const data = {
    loginId
  }
  return request({
    url: '/sales/info/resetPwdDefault',
    method: 'put',
    data: data
  })
}
