import request from '@/utils/request'

// 查询交接凭证-管道列表
export function listPipe(query) {
  return request({
    url: '/system/Cgpipe/list',
    method: 'get',
    params: query
  })
}

// 查询交接凭证-管道详细
export function getPipe(handoverPurchasePipeId) {
  return request({
    url: '/system/Cgpipe/' + handoverPurchasePipeId,
    method: 'get'
  })
}

// 新增交接凭证-管道
export function addPipe(data) {
  return request({
    url: '/system/Cgpipe',
    method: 'post',
    data: data
  })
}

// 修改交接凭证-管道
export function updatePipe(data) {
  return request({
    url: '/system/Cgpipe',
    method: 'put',
    data: data
  })
}

// 删除交接凭证-管道
export function delPipe(handoverPurchasePipeIds) {
  return request({
    url: '/system/Cgpipe/' + handoverPurchasePipeIds,
    method: 'delete'
  })
}

// 导出交接凭证-管道
export function exportPipe(query) {
  return request({
    url: '/system/Cgpipe/export',
    method: 'get',
    params: query
  })
}

// 确认过磅信息
export function updatePipeStatus(handoverPurchasePipeId){
  return request({
    url: '/system/Cgpipe/updatePipeStatus/' + handoverPurchasePipeId,
    method: 'get'
  })
}

// 交易明细列表
export function listDetail(query) {
  return request({
    url: '/system/pipe/detaillist',
    method: 'get',
    params: query
  })
}

// 交易明细列表
export function listPipeDe(query) {
  return request({
    url: '/system/CgpipeDe/list',
    method: 'get',
    params: query
  })
}

