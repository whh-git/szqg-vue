import request from '@/utils/request'

// 删除采购合同附件
export function delFile(sellContractProofId) {
    return request({
      url: '/sales/sellContract/' + sellContractProofId,
      method: 'delete'
    })
  }
  