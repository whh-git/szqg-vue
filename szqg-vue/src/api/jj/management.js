import request from '@/utils/request'

export function updateMove(entities) {
  return request({
    url: '/jj/management/moving',
    method: 'POST',
    data: entities
  })
}

// 查询banner管理列表
export function listManagement(query) {
  return request({
    url: '/jj/management/list',
    method: 'get',
    params: query
  })
}

// 查询banner管理详细
export function getManagement(bannerManagementId) {
  return request({
    url: '/jj/management/' + bannerManagementId,
    method: 'get'
  })
}

// 新增banner管理
export function addManagement(data) {
  return request({
    url: '/jj/management',
    method: 'post',
    data: data
  })
}

// 修改banner管理
export function updateManagement(data) {
  return request({
    url: '/jj/management',
    method: 'put',
    data: data
  })
}

// 删除banner管理
export function delManagement(bannerManagementId) {
  return request({
    url: '/jj/management/' + bannerManagementId,
    method: 'delete'
  })
}
