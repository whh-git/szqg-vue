import request from '@/utils/request'


// 获取待办列表
export function list() {
  return request({
    url: '/ProcessEngine/list/',
    method: 'get'
  })
}

// 创建流程
export function submitApply() {
  return request({
    url: '/ProcessEngine/submitApply',
    method: 'get'
  })
}
//提交
export function tySubmitWork(query) {
  return request({
    url: '/ProcessEngine/tySubmitWork',
    method: 'post',
    data: query
  })
}
// 提交流程
export function submitWork(query) {
  return request({
    url: `/ProcessEngine/submitWork`,
    method: 'post',
    data: query
  })
}

// 竞价确认
// submitWorkJJ
 export function submitWorkJJ(query) {
   return request({
     url: `/ProcessEngine/submitWorkJJ`,
     method: 'post',
     data: query
   })
 }

// 挂牌确认
// submitWorkGP
export function submitWorkGP(query) {
  return request({
    url: `/ProcessEngine/submitWorkGP`,
    method: 'post',
    data: query
  })
}

// 查看单个流程
export function ToDoOne(data) {
  return request({
    url: `/ProcessEngine/ToDoOne/`,
    method: 'post',
    data:data
  })
}

// 价格审批返回处理数据

export function jjBack(data) {
  return request({
    url: '/jj/priceApplyDetail/updatePriceByLc',
    method: 'post',
    data: data
  })
}

// 消息发送
export function ChatList(data) {
  return request({
    url: `/ProcessEngine/ChatList`,
    method: 'post',
    data:data
  })
}
// 计时
export function ToDoRefresh(data) {
  return request({
    url: `/ProcessEngine/ToDoRefresh`,
    method: 'post',
    data:data
  })
}
// 预览
export function getPic(data) {
  return request({
    url: `/ProcessEngine/getPic`,
    method: 'post',
    data:data
  })
}