import request from '@/utils/request'

// 查询价格申请列表
export function listPriceApply(query) {
  return request({
    url: '/jj/priceApply/list?transactionType=1',
    method: 'get',
    params: query
  })
}

// 挂牌
export function listPriceApply_2(query) {
  return request({
    url: '/jj/priceApply/list?transactionType=2',
    method: 'get',
    params: query
  })
}

// 政策定价
export function listPriceApply_3(query) {
  return request({
    url: '/jj/priceApply/list?transactionType=3',
    method: 'get',
    params: query
  })
}

// 查询价格申请详细
export function getPriceApply(priceApplyId) {
  return request({
    url: '/jj/priceApply/' + priceApplyId,
    method: 'get'
  })
}


// 查询站点信息
export function getOtherInfo() {
  return request({
    url: '/jj/priceApply/getOtherInfo',
    method: 'get'
  })
}

// 查询客户列表
export function findCustome() {
  return request({
    url: '/jj/priceApply/findCustome',
    method: 'get'
  })
}

// 查询站点列表
export function findSite() {
  return request({
    url: '/jj/priceApply/findSite',
    method: 'get'
  })
}

// 通过产品id查站点
export function getSiteInfoByPId(data) {
  return request({
    url: '/jj/priceApply/getSiteInfoByPId/'+data,
    method: 'get'
  })
}

// 新增价格申请
export function addPriceApply(data) {
  return request({
    url: '/jj/priceApply',
    method: 'post',
    data: data
  })
}
// 调价确认
export function insertjgPriceApplyByTJ(data) {
  return request({
    url: '/jj/priceApply/TJadd',
    method: 'post',
    data: data
  })
}

// 修改价格申请
export function updatePriceApply(data) {
  return request({
    url: '/jj/priceApply',
    method: 'put',
    data: data
  })
}

// 删除价格申请
export function delPriceApply(priceApplyId) {
  return request({
    url: '/jj/priceApply/' + priceApplyId,
    method: 'delete'
  })
}


// 新增 添加竞价招标
export function addpublish(data) {
  return request({
    url: '/jj/priceApply/startCallBid/',
    method: 'post',
    data:data
  })
}

// 挂牌发布
export function submit(data) {
  return request({
    url: '/jj/listed/',
    method: 'post',
    data:data
  })
}

// 查看公告
export function findnotice(data) {
  return request({
    url: '/jj/priceApply/findNotice/' + data,
    method: 'get',
    // data:data
  })
}

// 获取已通过价格委员会审批站点
export function getadd(priceApplyId) {
  return request({
    url: '/jj/callbid/getApplySiteInfo/'+priceApplyId,
    method: 'get'
  })
}

// 添加标段
export function addBD(data) {
  return request({
    url: '/jj/callbid/addBD',
    method: 'post',
    data:data
  })
}

// 页面列表
export function get_add() {
  return request({
    url: '/jj/callbid/getAdd',
    method: 'get'
  })
}

// 发布站点删除
export function delSite(data) {
  return request({
    url: '/jj/callbid/updateList',
    method: 'post',
    data:data
  })
}

// 上传文件
export function upload(data) {
  return request({
    url: '/jj/priceApply/upload',
    method: 'post',
    file:data
  })
}
// 查询价格类型
export function getProductInfo() {
  return request({
    url: '/jj/priceApply/getProductInfo',
    method: 'get'
  })
}

export function getCustomeList(data) {
  return request({
    url: '/jj/priceApply/getCustomeList',
    method: 'post',
    data:data
  })
}
