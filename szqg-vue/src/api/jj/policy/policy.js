import request from '@/utils/request'

// 查询政策定价列表
export function listPolicy(query) {
  return request({
    url: '/policy/policy/list',
    method: 'get',
    params: query
  })
}

// 查询政策定价详细
export function getPolicy(policyId) {
  return request({
    url: '/policy/policy/' + policyId,
    method: 'get'
  })
}

// 新增政策定价
export function addPolicy(data) {
  return request({
    url: '/policy/policy',
    method: 'post',
    data: data
  })
}

// 修改政策定价
export function updatePolicy(data) {
  return request({
    url: '/policy/policy',
    method: 'put',
    data: data
  })
}

// 删除政策定价
export function delPolicy(policyId) {
  return request({
    url: '/policy/policy/' + policyId,
    method: 'delete'
  })
}

// 获得站点信息
export function listSupplier() {
  return request({
    url: '/policy/policy/getPassApplyInfo',
    method: 'get'
  })
}

// 获取客户信息
export function getCus() {
  return request({
    url: '/policy/policy/getCus',
    method: 'get'
  })
}

export function getCustomeList(productId) {
  return request({
    url: '/policy/policy/getCustomeList/'+productId,
    method: 'get'
  })
}


// 提交客户信息
export function addTJ(data) {
  return request({
    url: '/policy/policy/addTJ',
    method: 'post',
    data:data
  })
}
