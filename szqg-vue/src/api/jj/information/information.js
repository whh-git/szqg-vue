import request from '@/utils/request'

//
export function deleteFileByPath(path) {
  return request({
    url: '/information/information/img/deleting',
    method: 'POST',
    params: { path }
  })
}

// 查询新闻动态列表
export function listInformation(query) {
  return request({
    url: '/information/information/list',
    method: 'get',
    params: query
  })
}

// 查询新闻动态详细
export function getInformation(newsInformationId) {
  return request({
    url: '/information/information/' + newsInformationId,
    method: 'get'
  })
}

// 新增新闻动态
export function addInformation(data) {
  return request({
    url: '/information/information',
    method: 'post',
    data: data
  })
}

// 修改新闻动态
export function updateInformation(data) {
  return request({
    url: '/information/information',
    method: 'put',
    data: data
  })
}

// 删除新闻动态
export function delInformation(newsInformationId) {
  return request({
    url: '/information/information/' + newsInformationId,
    method: 'delete'
  })
}
