import request from '@/utils/request'

// 查询挂牌招标列表
export function listListed(query) {
  return request({
    url: '/jj/listed/list',
    method: 'get',
    params: query
  })
}

// 查询挂牌招标详细
export function getListed(listedId) {
  return request({
    url: '/jj/listed/' + listedId,
    method: 'get'
  })
}

// 新增挂牌招标
export function addListed(data) {
  return request({
    url: '/jj/listed',
    method: 'post',
    data: data
  })
}

// 修改挂牌招标
export function updateListed(data) {
  return request({
    url: '/jj/listed',
    method: 'put',
    data: data
  })
}

// 删除挂牌招标
export function delListed(listedId) {
  return request({
    url: '/jj/listed/' + listedId,
    method: 'delete'
  })
}

// 确认
export function qd(data) {
  return request({
    url: '/jj/listed/priceCon',
    method: 'post',
    data:data
  })
}
