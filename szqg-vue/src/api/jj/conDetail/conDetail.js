import request from '@/utils/request'

// 查询合同交易明细列表
export function listConDetail(query) {
  return request({
    url: '/conDetail/conDetail/list',
    method: 'get',
    params: query
  })
}

// 查询合同交易明细详细
export function getConDetail(sellContractId) {
  return request({
    url: '/conDetail/conDetail/' + sellContractId,
    method: 'get'
  })
}

// 新增合同交易明细
export function addConDetail(data) {
  return request({
    url: '/conDetail/conDetail',
    method: 'post',
    data: data
  })
}

// 修改合同交易明细
export function updateConDetail(data) {
  return request({
    url: '/conDetail/conDetail',
    method: 'put',
    data: data
  })
}

// 删除合同交易明细
export function delConDetail(sellContractId) {
  return request({
    url: '/conDetail/conDetail/' + sellContractId,
    method: 'delete'
  })
}
