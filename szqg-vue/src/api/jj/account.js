import request from '@/utils/request'

// 查询价格台账列表 基础价格
export function listAccount(query) {
  return request({
    url: '/jj/account/list?type=1',
    method: 'get',
    params: query
  })
}

// 成交价格 列表
export function listAccount_2(query) {
  return request({
    url: '/jj/account/list?type=2',
    method: 'get',
    params: query
  })
}

// 查询价格台账详细
export function getAccount(priceAccountId) {
  return request({
    url: '/jj/account/' + priceAccountId,
    method: 'get'
  })
}

export function getAccount_2(priceAccountId) {
  return request({
    url: '/jj/account/getInfo/'+ priceAccountId,
    method: 'get'
  })
}
// /jj/account/getInfo/{priceAccountId}
// 新增价格台账
export function addAccount(data) {
  return request({
    url: '/jj/account',
    method: 'post',
    data: data
  })
}

// 导出价格台账
export function export_1(data) {
  return request({
    url: '/jj/account/export',
    method: 'get',
    params: data
  })
}

export function export_2(data) {
  return request({
    url: '/jj/account/export',
    method: 'get',
    params: data
  })
}


// 修改价格台账
export function updateAccount(data) {
  return request({
    url: '/jj/account',
    method: 'put',
    data: data
  })
}

// 删除价格台账
export function delAccount(priceAccountId) {
  return request({
    url: '/jj/account/' + priceAccountId,
    method: 'delete'
  })
}
