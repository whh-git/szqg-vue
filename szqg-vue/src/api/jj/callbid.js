import request from '@/utils/request'

// 查询竞价招标列表
export function listCallbid(query) {
  return request({
    url: '/jj/callbid/list',
    method: 'get',
    params: query
  })
}

// 查询竞价招标详细
export function getCallbid(callbidId) {
  return request({
    url: '/jj/callbid/' + callbidId,
    method: 'get'
  })
}

// 新增竞价招标
// export function addCallbid(data) {
//   return request({
//     url: '/jj/callbid',
//     method: 'post',
//     data: data
//   })
// }
export function addCallbid() {
  return request({
    url: '/jj/callbid',
    method: 'get',
  })
}

// 修改竞价招标
export function updateCallbid(data) {
  return request({
    url: '/jj/callbid',
    method: 'put',
    data: data
  })
}

// 删除竞价招标
export function delCallbid(callbidId) {
  return request({
    url: '/jj/callbid/' + callbidId,
    method: 'delete'
  })
}

// 授标
export function sb(data) {
  return request({
    url: '/jj/callbid/confirm',
    method: 'post',
    data: data
  })
}

// 弃标

export function bidabandonment(data) {
  return request({
    url: '/jj/callbid/BidAbandonment',
    method: 'post',
    data: data
  })
}

// 流标
export function liubiao(data) {
  return request({
    url: '/jj/callbid/liuBiao',
    method: 'post',
    data: data
  })
}

// 确认
export function pricecon(data) {
  return request({
    url: '/jj/callbid/priceCon',
    method: 'post',
    data: data
  })
}
//商务谈判
export function posttalks(data) {
  return request({
    url: '/jj/callbid/talks',
    method: 'post',
    data:data
  })
}