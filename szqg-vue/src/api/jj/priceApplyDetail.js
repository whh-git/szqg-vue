import request from '@/utils/request'

// 查询价格申请明细列表
export function listPriceApplyDetail(query) {
  return request({
    url: '/jj/priceApplyDetail/list',
    method: 'get',
    params: query
  })
}

// 查询价格申请明细详细
export function getPriceApplyDetail(priceApplyDetailId) {
  return request({
    url: '/jj/priceApplyDetail/' + priceApplyDetailId,
    method: 'get'
  })
}

// 新增价格申请明细
export function addPriceApplyDetail(data) {
  return request({
    url: '/jj/priceApplyDetail',
    method: 'post',
    data: data
  })
}

// 修改价格申请明细
export function updatePriceApplyDetail(data) {
  return request({
    url: '/jj/priceApplyDetail',
    method: 'put',
    data: data
  })
}

// 删除价格申请明细
export function delPriceApplyDetail(priceApplyDetailId) {
  return request({
    url: '/jj/priceApplyDetail/' + priceApplyDetailId,
    method: 'delete'
  })
}
