import request from '@/utils/request'

// 查询政策法规列表
export function listRegulation(query) {
  return request({
    url: '/regulation/regulation/list',
    method: 'get',
    params: query
  })
}

// 查询政策法规详细
export function getRegulation(regulationId) {
  return request({
    url: '/regulation/regulation/' + regulationId,
    method: 'get'
  })
}

// 新增政策法规
export function addRegulation(data) {
  return request({
    url: '/regulation/regulation',
    method: 'post',
    data: data
  })
}

// 修改政策法规
export function updateRegulation(data) {
  return request({
    url: '/regulation/regulation',
    method: 'put',
    data: data
  })
}

// 删除政策法规
export function delRegulation(regulationId) {
  return request({
    url: '/regulation/regulation/' + regulationId,
    method: 'delete'
  })
}

// 下载用户导入模板
export function importPdf(path) {
  return request({
    url: '/regulation/regulation/importPdf?path=' + path,
    method: 'get'
  })
}
