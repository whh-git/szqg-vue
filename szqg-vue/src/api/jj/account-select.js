import request from '@/utils/request'

// 获取曲线数据
export function findPriceCurve(productIds, siteId) {
  return request({
    url: '/jj/account/price-curves',
    method: 'get',
    params: { productIds, siteId }
  })
}

// 查询原油期货数据
export function findOilPrices() {
  return request({
    url: '/jj/account/oil-prices',
    method: 'get'
  })
}

// 查询所有的公司
export function companys() {
  return request({
    url: '/jj/account/companys',
    method: 'get'
  })
}

// 根据公司查询产品类型
export function findProductTypesByOrgId(orgId) {
  return request({
    url: '/jj/account/product-types',
    method: 'get',
    params: {
      orgId
    }
  })
}

// 查询所有的产品
export function findProductsByproductTypeId(orgId, productTypeId) {
  return request({
    url: '/jj/account/products',
    method: 'get',
    params: {
      orgId,
      productTypeId
    }
  })
}

// 查询站点
export function findSitesByProductId(orgId, productId) {
  return request({
    url: '/jj/account/sites',
    method: 'get',
    params: {
      orgId,
      productId
    }
  })
}

