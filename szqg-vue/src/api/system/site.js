import request from '@/utils/request'

// 查询站点列表
export function listSite(query) {
  return request({
    url: '/system/site/getSiteList',
    method: 'get',
    params: query
  })
}

// 查询站点详细
export function getSite(siteId) {
  return request({
    url: '/system/site/' + siteId,
    method: 'get'
  })
}

// 新增站点
export function addSite(data) {
  return request({
    url: '/system/site',
    method: 'post',
    data:data
  })
}

// 修改站点
export function updateSite(data) {
  return request({
    url: '/system/site',
    method: 'put',
    data: data
  })
}
// 修改储罐
export function updateTank(data) {
  return request({
    url: '/system/tank/update',
    method: 'put',
    data: data
  })
}

// 删除站点
export function delSite(siteIds) {
  return request({
    url: '/system/site/' + siteIds,
    method: 'delete'
  })
}
//删除储罐
export function  delTank(tankIds) {
  return request({
    url: '/system/tank/' + tankIds,
    method: 'delete'
  })
}

// 导出站点
export function exportSite(query) {
  return request({
    url: '/system/site/export',
    method: 'get',
    params: query
  })
}
//获取储罐列表
export function getTank(query){
  return request({
    url: '/system/tank/list',
    method: 'get',
    params: query
  })
}
//获取储罐详细信息
export function getTankInfo(tankId){
  return request({
    url: '/system/tank/info/' + tankId,
    method: 'get'
  })
}

// 新增储罐
export function addTank(data) {
  return request({
    url: '/system/tank',
    method: 'post',
    data:data
  })
}
export function getProductList(){
  return request({
    url: '/system/site/product',
    method: 'get'
  })
}