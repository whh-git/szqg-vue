import request from '@/utils/request'


export function getList(query) {
  return request({
    url: '/sales/stock/list',
    method: 'get',
    params: query
  })
}

export function getOrgNoStock() {
  return request({
    url: '/sales/stock/getOrgNoStock',
    method: 'get',
  })
}


export function getSiteByOrgId(orgId) {
  return request({
    url: '/sales/stock/getSiteByOrgId/' + orgId,
    method: 'get'
  })
}


export function createInventory(data) {
  return request({
    url: '/sales/stock/createStock',
    method: 'post',
    data: data
  })
}


export function getStockLog(query) {
  return request({
    url: '/sales/log/getStockLog',
    method: 'get',
    params: query
  })
}
