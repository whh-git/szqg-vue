import request from '@/utils/request'

// 查询采购合同列表
export function listContract(query) {
  return request({
    url: '/purchase/contract/list',
    method: 'get',
    params: query
  })
}

// 查询采购合同详细
export function getContract(purchaseContractId) {
  return request({
    url: '/purchase/contract/' + purchaseContractId,
    method: 'get'
  })
}

// 新增采购合同
export function addContract(data) {
  return request({
    url: '/purchase/contract',
    method: 'post',
    data: data
  })
}

// 修改采购合同
export function updateContract(data) {
  return request({
    url: '/purchase/contract',
    method: 'put',
    data: data
  })
}

// 删除采购合同
export function delContract(purchaseContractId) {
  return request({
    url: '/purchase/contract/' + purchaseContractId,
    method: 'delete'
  })
}

// 导出采购合同
export function exportContract(query) {
  return request({
    url: '/purchase/contract/export',
    method: 'get',
    params: query
  })
}

export function getProductByType(type) {
  return request({
    url: '/system/product/getProductByType/' + type,
    method: 'get'
  })
}

