import request from '@/utils/request'

// 销售汇总
export function getSalesCount() {
  return request({
    url: '/sales/index/getSalesCount',
    method: 'get',
  })
}

// 分公司销售盘点
export function getOrgSalesCount(data) {
  return request({
    url: '/sales/index/getOrgSalesCount',
    method: 'post',
    data:data
  })
}

export function getProductQuery() {
  return request({
    url: '/sales/index/getProductQuery',
    method: 'get'
  })
}

export function getSalesLayOut(data) {
  return request({
    url: '/sales/index/getSalesLayOut',
    method: 'post',
    data:data
  })
}

export function getSalesCountOne(data) {
  return request({
    url: '/sales/index/getSalesCountOne',
    method: 'post',
    data:data
  })
}

export function getSalesCountTwo(data) {
  return request({
    url: '/sales/index/getSalesCountTwo',
    method: 'post',
    data:data
  })
}

export function dealList() {
  return request({
    url: '/sales/index/dealList',
    method: 'post',
  })
}

export function getProductBenefit(data) {
  return request({
    url: '/sales/index/getProductBenefit',
    method: 'post',
    data:data
  })
}

export function getCustomerPurchases(data) {
  return request({
    url: '/sales/index/getCustomeBuy',
    method: 'post',
    data:data
  })
}



