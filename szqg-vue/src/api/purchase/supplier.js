import request from '@/utils/request'

// 查询供货方列表
export function listSupplier(query) {
  return request({
    url: '/purchase/supplier/list',
    method: 'get',
    params: query
  })
}

// 查询供货方详细
export function getSupplier(supplierId) {
  return request({
    url: '/purchase/supplier/' + supplierId,
    method: 'get'
  })
}

// 新增供货方
export function addSupplier(data) {
  return request({
    url: '/purchase/supplier',
    method: 'post',
    data: data
  })
}

// 修改供货方
export function updateSupplier(data) {
  return request({
    url: '/purchase/supplier',
    method: 'put',
    data: data
  })
}

// 删除供货方
export function delSupplier(supplierId) {
  return request({
    url: '/purchase/supplier/' + supplierId,
    method: 'delete'
  })
}

// 导出供货方
export function exportSupplier(query) {
  return request({
    url: '/purchase/supplier/export',
    method: 'get',
    params: query
  })
}

export function updateSupplierIdStatus(supplierId) {
  return request({
    url: '/purchase/supplier/updateSupplierIdStatus/' + supplierId,
    method: 'get'
  })
}
