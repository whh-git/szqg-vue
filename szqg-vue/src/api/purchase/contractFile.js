import request from '@/utils/request'

// 查询采购合同附件列表
export function listFile(query) {
  return request({
    url: '/purchase/contractFile/list',
    method: 'get',
    params: query
  })
}

// 查询采购合同附件详细
export function getFile(purchaseContractProofId) {
  return request({
    url: '/purchase/contractFile/' + purchaseContractProofId,
    method: 'get'
  })
}

// 新增采购合同附件
export function addFile(data) {
  return request({
    url: '/purchase/contractFile',
    method: 'post',
    data: data
  })
}

// 修改采购合同附件
export function updateFile(data) {
  return request({
    url: '/purchase/contractFile',
    method: 'put',
    data: data
  })
}

// 删除采购合同附件
export function delFile(purchaseContractProofId) {
  return request({
    url: '/purchase/contractFile/' + purchaseContractProofId,
    method: 'delete'
  })
}

// 导出采购合同附件
export function exportFile(query) {
  return request({
    url: '/purchase/contractFile/export',
    method: 'get',
    params: query
  })
}