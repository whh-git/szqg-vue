import Cookies from 'js-cookie'

const TokenKey = 'xsny_Admin-Token'
// const SessionKey = 'JSESSIONID'
const SessionKey = 'xsny'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setSession(token) {
  return Cookies.set(SessionKey, token)
}
