import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: 路由配置项
 *
 * hidden: true                   // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true               // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect           // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'             // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * meta : {
    roles: ['admin','editor']    // 设置该路由进入的权限，支持多个权限叠加
    title: 'title'               // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'             // 设置该路由的图标，对应路径src/icons/svg
    breadcrumb: false            // 如果设置为false，则不会在breadcrumb面包屑中显示
  }
 */

// 公共路由
export const constantRoutes = [{
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [{
      path: '/redirect/:path(.*)',
      component: (resolve) => require(['@/views/redirect'], resolve)
    }]
  },
  {
    path: '/login',
    component: (resolve) => require(['@/views/login'], resolve),
    hidden: true
  },
  {
    path: '/404',
    component: (resolve) => require(['@/views/error/404'], resolve),
    hidden: true
  },
  {
    path: '/401',
    component: (resolve) => require(['@/views/error/401'], resolve),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [{
      path: 'index',
      component: (resolve) => require(['@/views/index'], resolve),
      name: '首页',
      meta: {
        title: '首页',
        icon: 'dashboard',
        noCache: true,
        affix: true
      }
    }]
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [{
      path: 'profile',
      component: (resolve) => require(['@/views/system/user/profile/index'], resolve),
      name: 'Profile',
      meta: {
        title: '个人中心',
        icon: 'user'
      }
    }]
  },
  {
    path: '/dict',
    component: Layout,
    hidden: true,
    children: [{
      path: 'type/data/:dictId(.*)',
      component: (resolve) => require(['@/views/system/dict/data'], resolve),
      name: 'Data',
      meta: {
        title: '字典数据',
        icon: ''
      }
    }]
  },
  {
    path: '/system/role-auth',
    component: Layout,
    hidden: true,
    children: [{
      path: 'user/:roleId(.*)',
      component: (resolve) => require(['@/views/system/role/roleUser'], resolve),
      name: 'AuthUser',
      meta: {
        title: '分配用户',
        activeMenu: '/system/role'
      }
    }]
  },
  {
    path: '/system/site-auth',
    component: Layout,
    hidden: true,
    children: [{
      path: 'tank/:siteId(.*)',
      component: (resolve) => require(['@/views/system/site/indexTank'], resolve),
      name: 'AuthTank',
      meta: {
        title: '储罐信息维护',
        activeMenu: '/system/site'
      }
    }]
  },
  /**计量交接协议页面*/
  {
    path: '/sales/contract-agree',
    component: Layout,
    hidden: true,
    children: [{
      path: 'agreement/:contractId(.*)',
      component: (resolve) => require(['@/views/sales/contract/indexAgreement'], resolve),
      name: 'Infos',
      meta: {
        title: '计量交接协议',
        activeMenu: '/sales/contract'
      }
    }]
  },

  {
    path: '/sales/customer-user',
    component: Layout,
    hidden: true,
    children: [{
      path: 'customer/:customeId(.*)/:customeName(.*)',
      component: (resolve) => require(['@/views/sales/customer/indexLogin'], resolve),
      name: 'Info',
      meta: {
        title: '客户人员',
        activeMenu: '/sales/salesCustome/customer'
      }
    }]
  },

  {
    path: '/sales/customer-product',
    component: Layout,
    hidden: true,
    children: [{
      path: 'customeProduceIndex/:customeId(.*)',
      component: (resolve) => require(['@/views/sales/customer/customeProduceIndex'], resolve),
      name: 'customeProduceIndex',
      meta: {
        title: '客户产品入围管理',
        activeMenu: '/sales/salesCustome/customeProduceIndex'
      }
    }]
  },

  {
    path: '/sales/customer-file',
    component: Layout,
    hidden: true,
    children: [{
      path: 'customeFile/:customeId(.*)',
      component: (resolve) => require(['@/views/sales/customer/customeFile'], resolve),
      name: 'customeFile',
      meta: {
        title: '客户资质管理',
        activeMenu: '/sales/salesCustome/customeFile'
      }
    }]
  },

  {
    path: '/sales/deal',
    component: Layout,
    hidden: true,
    children: [{
      path: 'list',
      component: (resolve) => require(['@/views/jj/ProcessEngine/index'], resolve),
      name: 'Deal',
      meta: {
        title: '待办列表',
        activeMenu: '/jj/ProcessEngine/index'
      }
    }]
  },


  // {
  //       path: '/sales/customer-file',
  //       component: Layout,
  //       hidden: true,
  //       children: [
  //         {
  //           path: 'customer/:customeId(.*)/:customeName(.*)',
  //           component: (resolve) => require(['@/views/sales/customer/customeFile'], resolve),
  //           name: 'Info',
  //           meta: { title: '客户资质管理', activeMenu: '/sales/customeFile' }
  //         }
  //       ]
  //     },


  /**计量交接协议页面*/
  {
    path: '/sales/contract',
    component: Layout,
    hidden: true,
    children: [{
      path: 'agreement/:contractId(.*)',
      component: (resolve) => require(['@/views/sales/contract/indexAgreement'], resolve),
      name: 'Infos',
      meta: {
        title: '计量交接协议',
        activeMenu: '/sales/contract/agreement'
      }
    }]
  },


// 静态路由配置，跳转详情页及各个发起页面
  {
    path: '/gen',
    component: Layout,
    hidden: true,
    children: [{
      path: 'edit/:tableId(\\d+)',
      component: (resolve) => require(['@/views/tool/gen/editTable'], resolve),
      name: 'GenEdit',
      meta: {
        title: '修改生成配置'
      }
    }]
  },
// 竞价发起页面
  {
    path: '/publish',
    component: Layout,
    hidden: true,
    // redirect: 'noredirect',
    children: [{
      path: '/publish',
      component: (resolve) => require(['@/views/jj/publish/index'], resolve),
      name: 'publish',
      meta: {
        title: '发起竞价上报申请',
        // activeMenu: '/sales/priceManage/priceApply'
      }
    }]
  },

  // 竞价发布页面
  {
    path: '/t_publish',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [{
      path: '/t_publish',
      component: (resolve) => require(['@/views/jj/t_publish/index'], resolve),
      name: 'publish',
      meta: {
        title: '发布竞价招标',
        icon: 'user'
      }
    }]
  },

  // 挂牌上报发起页面
  {
    path: '/listing',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [{
      path: '/listing',
      component: (resolve) => require(['@/views/jj/listing/index'], resolve),
      name: 'publish',
      meta: {
        title: '发起挂牌上报申请',
        icon: 'user'
      }
    }]
  },

  // 挂牌上报发布页面
  {
    path: '/t_listing',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [{
      path: '/t_listing',
      component: (resolve) => require(['@/views/jj/t_listing/index'], resolve),
      name: 'publish',
      meta: {
        title: '发布挂牌招标',
        icon: 'user'
      }
    }]
  },

  // 政策发起
  {
    path: '/zc',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [{
      path: '/zc',
      component: (resolve) => require(['@/views/jj/zc/index'], resolve),
      name: 'publish',
      meta: {
        title: '政策定价发起',
        icon: 'user'
      }
    }]
  },


  {
    path: '/priceApplyDetail',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [{
      path: '/priceApplyDetail',
      component: (resolve) => require(['@/views/jj/priceApplyDetail/index'], resolve),
      name: 'notice',
      meta: {
        title: '查看进度',
        icon: 'user'
      }
    }]
  },

  // 竞价详情
  {
    path: '/j_info',
    component: Layout,
    redirect: 'noredirect',
    hidden: true,
    children: [{
        path: '/j_info',
        component: (resolve) => require(['@/views/jj/account/infor.vue'], resolve),
      }

    ]
  },

  // 挂牌详情
  {
    path: '/g_info',
    component: Layout,
    redirect: 'noredirect',
    hidden: true,
    children: [{
        path: '/g_info',
        component: (resolve) => require(['@/views/jj/listed/infor.vue'], resolve),
      }

    ]
  },


  // 详情2
  {
    path: '/jj_info',
    component: Layout,
    redirect: 'noredirect',
    hidden: true,
    children: [{
        path: '/jj_info',
        component: (resolve) => require(['@/views/jj/callbid/info.vue'], resolve),
      }

    ]
  },
  // 交易明细详情
  {
    path: '/con_infor',
    component: Layout,
    redirect: 'noredirect',
    hidden: true,
    children: [{
        path: '/con_infor',
        component: (resolve) => require(['@/views/jj/conDetail/infor.vue'], resolve),
      }

    ]
  },

  // 审批详情
  {
    path: '/pro_infor',
    component: Layout,
    redirect: 'noredirect',
    hidden: true,
    children: [{
        path: '/pro_infor',
        component: (resolve) => require(['@/views/jj/ProcessEngine/infor.vue'], resolve),
      },
       // 审批查看
       {
        path: '/pro_infory',
        component: (resolve) => require(['@/views/jj/ProcessEngine/infory.vue'], resolve),
      },
    ]
  },



  // 联动测试 可忽略
  // {
  //   path: '/test',
  //   component: Layout,
  //   hidden: false,
  //   redirect: 'noredirect',
  //   children: [
  //     {
  //       path: '/test',
  //       component: (resolve) => require(['@/views/jj/test/index'], resolve),
  //       name: 'test',
  //       meta: { title: '联动测试', icon: 'user' }
  //     }
  //   ]
  // },

  // 添加公告
  {
    path: '/add',
    component: Layout,
    redirect: 'noredirect',
    hidden: true,
    children: [{
        path: '/add',
        component: (resolve) => require(['@/views/jj/notice/add.vue'], resolve),
      }

    ]
  }
]

export default new Router({
  mode: 'history', // 去掉url中的#
  base: 'xsnyms',
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})
