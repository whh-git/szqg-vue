import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken } from '@/utils/auth'
import { setToken } from '@/utils/auth'
import { setSession } from '@/utils/auth'
NProgress.configure({ showSpinner: false })
const whiteList = ['/login', '/auth-redirect', '/bind', '/register']

router.beforeEach((to, from, next) => {
  if (to.query) {
    if (to.query.sessionId) {
      setSession(to.query.sessionId)
      delete to.query.sessionId
    }

    if (to.query.token) {
      setToken(to.query.token)
      delete to.query.token
    }
    // history.pushState({}, '', to.path)
  }

  NProgress.start()
  if (getToken()) {
  // if (true) {
    /* has token*/
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      if (store.getters.roles.length === 0) {
        // 判断当前用户是否已拉取完user_info信息
        store.dispatch('GetInfo').then(res => {
          // 拉取user_info
          const roles = res.roles
          store.dispatch('GenerateRoutes', { roles }).then(accessRoutes => {
            // 测试 默认静态页面
            // store.dispatch('permission/generateRoutes', { roles }).then(accessRoutes => {
            // 根据roles权限生成可访问的路由表
            router.addRoutes(accessRoutes) // 动态添加可访问路由表
            next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
          })
        })
          .catch(err => {
            store.dispatch('FedLogOut').then(() => {
              Message.error(err)
              next({ path: '/' })
            })
          })
      } else {
        next()
        // 没有动态改变权限的需求可直接next() 删除下方权限判断 ↓
        // if (hasPermission(store.getters.roles, to.meta.roles)) {
        //   next()
        // } else {
        //   next({ path: '/401', replace: true, query: { noGoBack: true }})
        // }
        // 可删 ↑
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      // window.location.href = 'http://10.78.9.246/qgsalesysapi/index'
      // window.location.href = 'http://localhost:20202/qgsalesys'
      next(`/login?redirect=${to.fullPath}`) // 否则全部重定向到登录页
      // next(`http://127.0.0.1:8080?redirect=${to.fullPath}`) // 否则全部重定向到登录页
      // window.location.href = 'http://192.168.42.15/cas/login?service=http://www.server.com:8080/web/login/cas&url=www.baidu.com'
      // window.location.href = 'http://127.0.0.1:20208/cas/login?service=http://www.server.com:8080/web/login/cas&url=www.baidu.com'
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
